console.log("Hello World");

//Arrays
//Arrays are used to store multiple related data/values in a single variable
//It is created/declared using [] brackets also known as "Array Literals"


let hobbies = ["Play video games", "Read a book", "Listen to music"];

//Arrays make it easy to manage, manipulate a set of data
//Arrays have different methods/functions that allow us to manage our array

//Methods are functions associated with an object
//arrays are actually a special type of object

console.log(typeof hobbies);

let grades = [75.4, 98.6, 90.34, 91.50];
const planets = ["Mercury", "Venus", "Mars", "Earth"];

//alternative way to write arrays
let myTasks =[
	"drink html",
	"eat javascript",
	"inhale css",
	'bake react'
	];

//Arrays as a best practice contains values of the same type

let arraySample = ["Saitama", "One Punch Man", 25000, true];
console.log(arraySample);

//however, since there are no problems in creating arrays like this, you may encounter exceptions to this rule in the future and in fact in other JS libraries or frameworks

//array as a collection of data, has methods to manipulate and manage the array
//Having values with different data types might interfere or conflict with the methods of an array

/*

	Mini Activity
	1. Create a variable that will store an array of at least 5 of your daily routine or task
	2. Create a variable which will store at least 4 capital cities in the world
	3. Log the variables in the the console and send a ss in our hangouts

*/

let myRoutine = ["Code", "Work", "Eat", "Lift", "Repeat"];
let capCities = ["Manila", "Tokyo", "Washington DC", "Paris"];

console.log(myRoutine);
console.log(capCities);	

//each item in an array is called an element
//array as a collection of data, as a convention, its name is usually plural

//we can also add the values of variables as elements in an array
let username1 = "pink_princess";
let username2 = "DareAngel";
let username3 = "WonderfulEgg";
let username4 = "mahalparinkita_123";

let guildMembers = [username1, username2, username3, username4];

console.log(guildMembers);

//.length property
//the .length property of an array tells about the number of elements inside our array
//it can actually be set and manipulated
//the .length property of an array is a number type

console.log(myRoutine.length);
console.log(capCities.length);

//in fact, even strings have a .length property, which tells us the number of characters in a string are able to use some array methods and properties
//whitespaces are counted as characters

let fullName = "Cardo Dalisay";
console.log(fullName.length);

//we can manipulate the .length property of an array. Being that .length property is a number that tells the total number of elements in an array
//we can actually delete the last item in an array by manipulating the .length property

myRoutine.length = myRoutine.length-1;
console.log(myRoutine.length);
console.log(myRoutine);

//we could also decrement the .length property of an array

capCities.length--;
console.log(capCities);

//can we do the same trick with a string?
//Not possible
fullName.length = fullName.length-1;
console.log(fullName);//Cardo Dalisay

//can we also add or lengthen using the same trick?

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

theBeatles[4] = "Cardo";
console.log(theBeatles);
theBeatles[theBeatles.length] = "Ely";
console.log(theBeatles);
theBeatles[theBeatles.length] = "Chito";
theBeatles[theBeatles.length] = "MJ";

console.log(theBeatles);
console.log(theBeatles.length);

//if we want to access a particular item in the array, we can do so with array indices. Each item are ordered according to their index
//NOTE: index are number types

console.log(capCities[0]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);//Shaq
console.log(lakersLegends[3]);//Magic

//we can also save/store a particular array element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//we can also update/re-assign the array elements using their index

lakersLegends[2] = "Pau";
console.log(lakersLegends);

let favoriteFoods = [
	"Tonkatsu",
	"Adobo",
	"Pizza",
	"Lasagna",
	"Sinigang"
	];


favoriteFoods[3] = "Kare-kare";
favoriteFoods[4] = "Bulalo";

console.log(favoriteFoods);



let theTrainers = ['Ash'];

function addTrainers(trainer){
	theTrainers[theTrainers.length] = trainer;
}

addTrainers('Brock');
addTrainers('Misty');
console.log(theTrainers);

function findBlackMamba(index) { 
	return lakersLegends[index];

}	

let blackMamba = findBlackMamba(0);
console.log(blackMamba);


//Accessing the last element of the array
let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegend.length-1;
console.log(lastElementIndex);//4
console.log(bullsLegend.length);//5

console.log(bullsLegend[lastElementIndex]);//Kukoc
console.log(bullsLegend[bullsLegend.length-1]);//Kukoc

//Adding items in our array

let newArr = [];
console.log(newArr[0]);//undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);//undefined

newArr[1] = 'Tifa Lockhart';
console.log(newArr);

newArr[1] = "Aerith Gainsborough";
console.log(newArr);


newArr[newArr.length] = 'Barret Wallace';
console.log(newArr);

newArr[newArr.length-1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length-2] = "Squall Leonhart";
console.log(newArr);

//Looping over an array
//we can loop over an array and iterate all items in the array
//set count as the index and set a condition that as the current index iterated is less than the length of the array
//it is set this way because the index starts at 0


//[0,1,2]
for (let index=0; index<newArr.length; index++){
	console.log(newArr[index]);

}

let numArr = [5, 12, 30, 46, 40];

//check each item in the array if they are divisible by 5 or not

for(let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisble by 5");
	}else {
		console.log(numArr[index] + ' is not divisible by 5');
	}
}

//Multidimensional Arrays

/*
	- Multidimensional Arrays are useful for storing complex data structures
	-a practical application of this is to help visualize/create real world objects
*/

let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];

console.log(chessBoard);

//access elements in a multidimensional array

console.log(chessBoard[1][4]);//e2

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);
